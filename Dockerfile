FROM node:18-alpine AS deps

LABEL org.opencontainers.image.authors="Dmytro Serbeniuk"
LABEL version="1.0"
LABEL description="Docker for Redemption"

RUN npm i -g pnpm
WORKDIR /app

COPY package*.json pnpm-lock.yaml ./
ARG NODE_ENV
ENV NODE_ENV $NODE_ENV


# install dependencies
RUN pnpm install

###########
# BUILDER #
###########

FROM deps AS build

WORKDIR /app

ENV NEXT_TELEMETRY_DISABLED 1RUN
# copy project
COPY . .
COPY --from=deps /app/node_modules ./node_modules

RUN pnpm prisma generate
RUN pnpm build



#########
# FINAL #
#########

# pull official base image node:18-alpine
FROM deps AS deploy

# create the appropriate directories
WORKDIR /app

# create the app user
RUN addgroup -S nextjs && adduser -S -G nextjs nextjs \
     && chown nextjs:nextjs -R /app

# change to the nextjs user
USER nextjs

ENV NODE_ENV development

# copy project from build step
COPY --chown=nextjs:nextjs .env ./
COPY --from=build --chown=nextjs:nextjs /app/pages ./pages
COPY --from=build --chown=nextjs:nextjs /app/public ./public
COPY --from=build --chown=nextjs:nextjs /app/package.json ./
COPY --from=build --chown=nextjs:nextjs /app/.next ./.next
COPY --from=build --chown=nextjs:nextjs /app/app ./app
COPY --from=build --chown=nextjs:nextjs /app/assets ./assets
COPY --from=build --chown=nextjs:nextjs /app/components ./components
COPY --from=build --chown=nextjs:nextjs /app/config ./config
COPY --from=build --chown=nextjs:nextjs /app/content ./content
COPY --from=build --chown=nextjs:nextjs /app/hooks ./hooks
COPY --from=build --chown=nextjs:nextjs /app/lib ./lib
COPY --from=build --chown=nextjs:nextjs /app/prisma ./prisma
COPY --from=build --chown=nextjs:nextjs /app/styles ./styles
COPY --from=build --chown=nextjs:nextjs /app/types ./types
COPY --from=build --chown=nextjs:nextjs /app/.commitlintrc.json  ./.commitlintrc.json
COPY --from=build --chown=nextjs:nextjs /app/.editorconfig  ./.editorconfig
COPY --from=build --chown=nextjs:nextjs /app/.eslintrc.json  ./.eslintrc.json
COPY --from=build --chown=nextjs:nextjs /app/contentlayer.config.js  ./contentlayer.config.js
COPY --from=build --chown=nextjs:nextjs /app/middleware.ts  ./middleware.ts
COPY --from=build --chown=nextjs:nextjs /app/next-env.d.ts  ./next-env.d.ts
COPY --from=build --chown=nextjs:nextjs /app/next.config.mjs  ./next.config.mjs
COPY --from=build --chown=nextjs:nextjs /app/postcss.config.js  ./postcss.config.js
COPY --from=build --chown=nextjs:nextjs /app/prettier.config.js  ./prettier.config.js
COPY --from=build --chown=nextjs:nextjs /app/tailwind.config.js  ./tailwind.config.js
COPY --from=build --chown=nextjs:nextjs /app/tsconfig.json ./tsconfig.json
COPY --from=build --chown=nextjs:nextjs /app/node_modules ./node_modules

# Run and expose the server on port 3000
EXPOSE 3000

#run pnpm dev
CMD [ "pnpm", "dev" ]


# FROM nginx:stable-alpine3.17 AS nginx

# RUN rm /etc/nginx/conf.d/default.conf
# COPY nginx.conf /etc/nginx/conf.d
