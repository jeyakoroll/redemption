import Image from "next/image"
import logo from "@/public/images/logo.svg"

import { siteConfig } from "@/config/site"
import { Icons } from "@/components/icons"

export function SiteFooter() {
  return (
    <footer className="text-base">
      <hr className="border-b-1 mx-auto mb-4 w-1/4 border-slate-700	" />
      <div className="mx-auto flex w-fit space-x-3">
        <a href="#">Що таке церква Христова?</a>
        <span className="font-bold"></span>
        <a href="#">Як я можу бути спасенним?</a>
      </div>
      <hr className="mx-auto my-4 w-48 border-t-4 border-double border-slate-700	" />
      <div className="mx-auto w-1/3">
        <Image className="mx-auto w-2/6" src={logo} alt="logo" />
        <p className="mx-auto w-fit	 text-center text-sm md:text-left">
          м. Мукачево, Україна 68000
        </p>
        <p className="mx-auto mt-4 w-fit">
          <a
            href={siteConfig.links.instagram}
            target="_blank"
            rel="noreferrer"
            className="font-medium underline underline-offset-4"
          >
            <Icons.instagram className="mb-5" />
          </a>
        </p>
      </div>
          <div>
            <p className="text-center">
              Here will be the address 
            </p>
          </div>
        <a href="tel:">Here - a phone number</a>
    </footer>
  )
}
