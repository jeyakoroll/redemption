import { MarketingConfig } from "types"

export const marketingConfig: MarketingConfig = {
  mainNav: [
    {
      title: "Main",
      href: "/",
    },
    {
      title: "Classes",
      href: "/blog",
    },
    {
      title: "Calendar",
      href: "/pricing",
    },
    {
      title: "Articles",
      href: "/docs",
    },
    {
      title: "Videos",
      href: "/contact",
    },
    {
      title: "Ministries",
      href: "/contact",
    },
    {
      title: "Staff",
      href: "/staff",
    },
    {
      title: "Contact",
      href: "/contact",
      disabled: true,
    },
  ],
}

