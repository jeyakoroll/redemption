"use client"

import React, { createContext, useRef, useState } from "react"
import Image from "next/image"
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react"

// Import Swiper styles
import "swiper/css"
import "swiper/css/navigation"
import "swiper/css/pagination"

import "../../../styles/styles.css"
import exp from "constants"
// import required modules
import { History, Navigation, Pagination } from "swiper"

import champion from "../../../public/images/champions.jpg"
import hero from "../../../public/images/hero.png"
import logo from "../../../public/images/logo.jpg"
import rescue from "../../../public/images/resurrected.jpg"
import youth from "../../../public/images/young.jpg"

export function Slider() {
  return (
    <>
      <Swiper
        spaceBetween={15}
        slidesPerView={2}
        navigation={true}
        pagination={true}
        history={{
          key: "slide",
        }}
        modules={[Navigation, Pagination, History]}
        className="mySwiper"
      >
        
        <SwiperSlide data-history="1">
          <Image src={rescue} width={280} alt="Rescue image" priority />
        </SwiperSlide>
        <SwiperSlide data-history="Slide 2">
          <Image src={youth} width={250} alt="Youth image" priority />
        </SwiperSlide>
        <SwiperSlide data-history="3">
          <Image src={champion} width={250} alt="Hero image" priority />
        </SwiperSlide>
        <SwiperSlide data-history="Slide 4">
          <Image src={logo} width={250} alt="Hero image" priority />
        </SwiperSlide>
        <SwiperSlide data-history="5">
          <Image src={hero} width={250} alt="Hero image" priority />
        </SwiperSlide>
        <SwiperSlide data-history="Slide 6">Slide 6</SwiperSlide>
        <SwiperSlide data-history="7">Slide 7</SwiperSlide>
        <SwiperSlide data-history="Slide 8">Slide 8</SwiperSlide>
        <SwiperSlide data-history="9">Slide 9</SwiperSlide>
      </Swiper>
    </>
  )
}
